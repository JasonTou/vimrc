" 设置 leader
let mapleader=","
"  DirectX rendering
" set rop=type:directx,gamma:1.0,contrast:0.5,level:1,geom:1,renmode:4,taamode:1

set nocompatible
source $VIMRUNTIME/vimrc_example.vim
source $VIMRUNTIME/mswin.vim
behave mswin

set diffexpr=MyDiff()
function MyDiff()
  let opt = '-a --binary '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  let eq = ''
  if $VIMRUNTIME =~ ' '
    if &sh =~ '\<cmd'
      let cmd = '""' . $VIMRUNTIME . '\diff"'
      let eq = '"'
    else
      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
    endif
  else
    let cmd = $VIMRUNTIME . '\diff'
  endif
  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
endfunction

filetype off

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'scrooloose/nerdtree'
Plugin 'tpope/vim-fugitive'
Plugin 'EasyMotion'
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'junegunn/fzf'
Plugin 'ctrlp.vim'
Plugin 'luochen1990/rainbow'
Plugin 'tomasr/molokai'
Plugin 'altercation/vim-colors-solarized'
Plugin 'a.vim'

" Plugin 'Valloric/YouCompleteMe'
" Plugin 'taglist.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Put your non-Plugin stuff after this line

" 配色
" colorscheme evening
" colorscheme molokai
colorscheme solarized

" 显示行号
set nu
" 不生成备份文件
set nobk
" 不生成临时文件
set noswapfile
" 不生成独立的 undo 文件
set noudf

" 缩进
set tabstop=4
set shiftwidth=4
set cindent

noremap <F5> :tabp<return>
noremap <F6> :tabn<return>
" noremap <esc> :noh<return><esc>
noremap <F12> :call Toggle16mode()<return>

" 搜索时忽略大小写
set ignorecase
" set backupdir=D:/VIM_SWP_FILE
" set directory=D:/VIM_SWP_FILE

" 支持GBK等编码的显示
set fileencodings=utf-8,gb18030,gbk,gb2312,big5
set termencoding=utf-8
set fileencoding=utf-8
set enc=utf-8

" 解决菜单乱码  
source $VIMRUNTIME/delmenu.vim  
source $VIMRUNTIME/menu.vim  
  
" 解决consle输出乱码  
language messages zh_CN.utf-8  

" 最多 40 个 TAB 标签页
set tabpagemax=40

" rainbow
let g:rainbow_active = 1

" colorscheme
let g:solarized_termtrans=1
let g:solarized_contrast="normal"
let g:solarized_visibility="normal"

" airline
" if !exists('g:airline_symbols')
" 	let g:airline_symbols = {}
" endif

" let g:airline_left_sep = '▶'
" let g:airline_left_alt_sep = '❯'
" let g:airline_right_sep = '◀'
" let g:airline_right_alt_sep = '❮'
" let g:airline_symbols.linenr = '¶'
" let g:airline_symbols.branch = '⎇'

" 是否打开tabline
" let g:airline#extensions#tabline#enabled = 1

let g:airline_theme="solarized"
